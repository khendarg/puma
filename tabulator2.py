#!/usr/bin/env python

import os
import re
import argparse
from kevtools_common.types import TCID

def parse_table(fh):
    keys = []
    table = []
    for l in fh:
        if l.startswith('#'): keys = l[1:].strip().split('\t')
        else:
            table.append(dict([(k, v) for k, v in zip(keys, l.strip().split('\t'))]))
    return table

def parse_sftable(fh):
    table = {}
    for l in fh:
        if l.startswith('#'): continue
        sl = l.strip().split('\t')

        v = {'short': sl[3], 'full': sl[4]}
        for i in range(3):
            table[TCID(sl[i].upper().replace('..', '').replace('-', ''))] = v

    return table

def sanitize(s):
    return re.sub('[^-0-9A-Za-z_]', 'x',s)

def summarize_tables(tables, superfamilies, level=4):
    contigs = {}
    contignames = set()
    for name in tables:
        for gene in tables[name]:
            contigname = TCID(gene['tcid'])[:level]
            contignames.add(TCID(gene['tcid'])[:level])

    for contigname in contignames:
        contig = Contig()
        contigs[contigname] = contig
        contig['subfamily_presence'] = 0
        contig['total_contigs'] = len(contignames)
        contig['Avg_paralogs'] = 0
        contig['Max_num_paralogs'] = 0
        contig['Num_contributing_genomes'] = 0
        contig['TC-subclass'] = ''
        try: contig['Superfamily'] = sanitize(superfamilies[contigname]['short'])
        except KeyError: contig['Superfamily'] = 'Unclassified'
        contig['Superfamily_size'] = 0
        contig['Presence_absence'] = 0
        for genome in tables:
            contig[genome] = 0
            if contigname not in contigs: contigs[contigname] = {}
            contig = contigs[contigname]
            contig['contig'] = contigname
            contig['TC-subclass'] = contigname[:2]
            #for name in tables: contig[name] = 0

    sfsizes = {}
    for genome in tables:
        for gene in tables[genome]:
            contig = contigs[TCID(gene['tcid'])[:level]]
            if contig['Superfamily'] not in sfsizes: sfsizes[contig['Superfamily']] = 0
            sfsizes[contig['Superfamily']] += 1
    for tcid in contigs:
        contigs[tcid]['Superfamily_size'] = sfsizes[contigs[tcid]['Superfamily']]

    for genome in tables:
        for gene in tables[genome]:
            tcid = TCID(gene['tcid'])[:level]

            contig = contigs[tcid]
            contig[genome] += 1
            contig['Avg_paralogs'] += 1
            # contig['

    for gene in contigs:
        contig = contigs[gene]

        contigs[gene]['Num_contributing_genomes'] = sum([contig[genome] > 0 for genome in tables])
        contigs[gene]['Avg_paralogs'] /= contigs[gene]['Num_contributing_genomes']
        for genome in tables:
            if contig[genome]: contig[genome] = 1
            else: contig[genome] = 0
        contig['Presence_absence'] = sum([(contig[genome] << i) for i, genome in enumerate(tables)])

            #contigs[gene][genome] = 1
    return contigs

def print_summary(contigs, order):
    out = ''
    firstrow = True
    for gene in contigs:
        contig = contigs[gene]
        if firstrow:
            firstrow = False
            out += '\t'.join(order) + '\n'
        out += '\t'.join([str(contig[k]) for k in order]) + '\n'
    return out

class Contig(dict):
    def __lt__(self, other): return sf_and_presence(self, other)

def sf_and_presence(contig1, contig2):
    #different superfamily sizes: privilege the larger
    if contig1['Superfamily_size'] < contig2['Superfamily_size']: return True
    elif contig1['Superfamily_size'] > contig2['Superfamily_size']: return False

    #different number of genomes: privilege the most filled-in
    if contig1['Num_contributing_genomes'] < contig2['Num_contributing_genomes']: return True
    elif contig1['Num_contributing_genomes'] > contig2['Num_contributing_genomes']: return False

    #different distribution: use the lazy binarization order
    if contig1['Presence_absence'] < contig2['Presence_absence']: return True
    elif contig1['Presence_absence'] > contig2['Presence_absence']: return False

    #different TC-ID: use alphabetical order (this ought to be unique...)
    if contig1['contig'] < contig2['contig']: return True
    elif contig1['contig'] > contig2['contig']: return False
    else: raise ValueError('Non-unique contig id!')
    
def print_order(contigs, sorter=None):
    return [contig['contig'] for contig in sorted(contigs.values())]


def main(args):
    tables = {}
    for fn in args.infile:
        name = os.path.basename(fn).replace('.tsv', '').title()
        with open(fn) as fh:
            tables[name] = parse_table(fh)
    with open(args.s) as fh:
        superfamilies = parse_sftable(fh)

    summary = summarize_tables(tables, superfamilies, level=args.l)
    order = ['contig',
            'total_contigs',
            'subfamily_presence',
            'Avg_paralogs',
            'Max_num_paralogs',
            'Num_contributing_genomes',
            'TC-subclass',
            'Superfamily',
            'Superfamily_size',
            'Presence_absence',
            ] + [genome for genome in tables]

    with open(args.d, 'w') as fh:
        fh.write(print_summary(summary, order))

    with open(args.t, 'w') as fh:
        fh.write('\n'.join([str(x) for x in print_order(summary, 'sf_and_presence')]))


if __name__ == '__main__':
    parser = argparse.ArgumentParser()

    parser.add_argument('-d', required=True)
    parser.add_argument('-t', required=True)
    parser.add_argument('-l', type=int, default=4, help='Smallest TC-level (default: 4)')
    parser.add_argument('-s', help='Superfamily lookup table')
    parser.add_argument('infile', nargs='+')

    args = parser.parse_args()

    main(args)
